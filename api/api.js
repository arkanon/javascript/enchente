
// Creates a simple polygon given its vertex coordinates

function initMap()
{



  // Create a new StyledMapType object, passing it an array of styles, and the name to be displayed on the map type control
  const styledMapType = new google.maps.StyledMapType
  (
    [
      {                                                 elementType: "geometry"           , stylers: [{ color: "#000000" }], },
      {                                                 elementType: "labels.text.fill"   , stylers: [{ color: "#523735" }], },
      {                                                 elementType: "labels.text.stroke" , stylers: [{ color: "#f5f1e6" }], },
      { featureType: "administrative"                 , elementType: "geometry.stroke"    , stylers: [{ color: "#c9b2a6" }], },
      { featureType: "administrative.land_parcel"     , elementType: "geometry.stroke"    , stylers: [{ color: "#dcd2be" }], },
      { featureType: "administrative.land_parcel"     , elementType: "labels.text.fill"   , stylers: [{ color: "#ae9e90" }], },
      { featureType: "landscape.natural"              , elementType: "geometry"           , stylers: [{ color: "#dfd2ae" }], },
      { featureType: "poi"                            , elementType: "geometry"           , stylers: [{ color: "#dfd2ae" }], },
      { featureType: "poi"                            , elementType: "labels.text.fill"   , stylers: [{ color: "#93817c" }], },
      { featureType: "poi.park"                       , elementType: "geometry.fill"      , stylers: [{ color: "#a5b076" }], },
      { featureType: "poi.park"                       , elementType: "labels.text.fill"   , stylers: [{ color: "#447530" }], },
      { featureType: "road"                           , elementType: "geometry"           , stylers: [{ color: "#f5f1e6" }], },
      { featureType: "road.arterial"                  , elementType: "geometry"           , stylers: [{ color: "#fdfcf8" }], },
      { featureType: "road.highway"                   , elementType: "geometry"           , stylers: [{ color: "#ff0000" }], },
      { featureType: "road.highway"                   , elementType: "geometry.stroke"    , stylers: [{ color: "#e9bc62" }], },
      { featureType: "road.highway.controlled_access" , elementType: "geometry"           , stylers: [{ color: "#e98d58" }], },
      { featureType: "road.highway.controlled_access" , elementType: "geometry.stroke"    , stylers: [{ color: "#db8555" }], },
      { featureType: "road.local"                     , elementType: "labels.text.fill"   , stylers: [{ color: "#806b63" }], },
      { featureType: "transit.line"                   , elementType: "geometry"           , stylers: [{ color: "#dfd2ae" }], },
      { featureType: "transit.line"                   , elementType: "labels.text.fill"   , stylers: [{ color: "#8f7d77" }], },
      { featureType: "transit.line"                   , elementType: "labels.text.stroke" , stylers: [{ color: "#ebe3cd" }], },
      { featureType: "transit.station"                , elementType: "geometry"           , stylers: [{ color: "#dfd2ae" }], },
      { featureType: "water"                          , elementType: "geometry.fill"      , stylers: [{ color: "#0000ff" }], },
      { featureType: "water"                          , elementType: "labels.text.fill"   , stylers: [{ color: "#92998d" }], },
    ],
    { name: "Styled Map" },
  );

  // Create a map object, and include the MapTypeId to add to the map type control
  const map = new google.maps.Map(document.getElementById("map"),
  {
    zoom   : 11,
    center : { lng: -51.930 , lat: -29.375 },
    mapTypeControlOptions :
    {
      mapTypeIds:
      [
        "roadmap"   , // mapa
        "satellite" , // satélite
        "hybrid"    , // satélite + marcadores
        "terrain"   , // mapa + relevo
        "styled_map"  // styled map
      ],
    },
 // mapTypeId : "satellite",
  });

  // Associate the styled map with the MapTypeId and set it to display
  map.mapTypes.set("styled_map", styledMapType);
  map.setMapTypeId("terrain");



  const input             = document.getElementById("pac-input");
  const latlng            = document.getElementById("latlng");
  const infowindowContent = document.getElementById("infowindow-content");

  const infowindow   = new google.maps.InfoWindow();
  const marker       = new google.maps.Marker({ map: map });
  const geocoder     = new google.maps.Geocoder();
  const autocomplete = new google.maps.places.Autocomplete
  (
    input,
    {
      fields: ["place_id", "geometry", "formatted_address", "name"],
    }
  );

  autocomplete.bindTo("bounds", map);

  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(latlng);

  infowindow.setContent(infowindowContent);

  latlng.addEventListener
  (
    "keypress", (e) =>
    {
      if (e.key === "Enter") { geocodeLatLng(geocoder, map, infowindow) };
    }
  );

  marker.addListener
  (
    "click", () =>
    {
      infowindow.open(map, marker);
    }
  );

  autocomplete.addListener
  (
    "place_changed", () =>
    {

      infowindow.close();

      const place = autocomplete.getPlace();

      if (!place.geometry || !place.geometry.location)
      {
        return;
      }

      if (place.geometry.viewport)
      {
        map.fitBounds(place.geometry.viewport);
      }
      else
      {
        map.setCenter(place.geometry.location);
        map.setZoom(17);
      }

      // Set the position of the marker using the place ID and location.
      // @ts-ignore This should be in @typings/googlemaps.
      marker.setPlace
      (
        {
          placeId: place.place_id,
          location: place.geometry.location,
        }
      );
      marker.setVisible(true);
      infowindowContent.children.namedItem("place-name"   ).textContent = place.name;
      infowindowContent.children.namedItem("place-id"     ).textContent = place.place_id;
      infowindowContent.children.namedItem("place-address").textContent = place.formatted_address;
      infowindow.open(map, marker);

    }
  );



  // Define the LatLng coordinates for the polygon's path
  const polyCoords =
  [
    { lng: -51.79516604123000 , lat: -29.13083332464456 },
    { lng: -51.91511575212842 , lat: -29.10941247084805 },
    { lng: -52.04032548063314 , lat: -29.61685460368627 },
    { lng: -51.91339462328989 , lat: -29.64118168407045 },
    { lng: -51.79516604123000 , lat: -29.13083332464456 },
  ];

  // Construct the polygon
  const poly = new google.maps.Polygon(
  {
    paths         : polyCoords,
    strokeColor   : "#ff0000",
    strokeOpacity : 0.8,
    strokeWeight  : 0,
    fillColor     : "#ff0000",
    fillOpacity   : 0.2,
  });

  poly.setMap(map);



}



function geocodeLatLng(geocoder, map, infowindow)
{

  const input = document.getElementById("latlng").value;
  const latlngStr = input.split(",", 2);

  const latlng =
  {
    lat: parseFloat(latlngStr[0]),
    lng: parseFloat(latlngStr[1]),
  };

  geocoder
    .geocode({ location: latlng })
    .then((response) => {
      if (response.results[0]) {
        map.setZoom(11);

        const marker = new google.maps.Marker({
          position: latlng,
          map: map,
        });

        infowindow.setContent(response.results[0].formatted_address);
        infowindow.open(map, marker);
      } else {
        window.alert("No results found");
      }
    })
    .catch((e) => window.alert("Geocoder failed due to: " + e));

}



window.initMap = initMap;



// EOF
