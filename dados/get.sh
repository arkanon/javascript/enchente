#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2024/06/19 (Wed) 16:22:14 -03

  pref=https://nivelguaiba.com.br/lajeado

  wget $pref.60days.json -O lajeado-60d-20240425.083000-20240619.154500.json
  wget $pref.30days.json -O lajeado-30d-20240520.080000-20240619.154500.json
  wget $pref.15days.json -O lajeado-15d-20240604.080000-20240619.154500.json
  wget $pref.7days.json  -O lajeado-07d-20240612.080000-20240619.154500.json
  wget $pref.3days.json  -O lajeado-03d-20240616.000000-20240619.154500.json
  wget $pref.1day.json   -O lajeado-01d-20240618.154500-20240619.154500.json

# EOF
