# enchente

Área afetada pela enchente de acordo com a cota atingida pelo rio.



## Monitoramento do Nível do Rio Taquari

IPH - Instituto de Pesquisas Hidráulicas da UFRGS  
(Dados coletados de 15 em 15 minutos e disponibilizados de hora em hora.)

- http://nivelguaiba.com.br/lajeado
<!-- -->
- http://nivelguaiba.com.br/lajeado.60days.json
- http://nivelguaiba.com.br/lajeado.30days.json
- http://nivelguaiba.com.br/lajeado.15days.json
- http://nivelguaiba.com.br/lajeado.7days.json
- http://nivelguaiba.com.br/lajeado.3days.json
- http://nivelguaiba.com.br/lajeado.1day.json

SGB - Serviço Geológico do Brasil

- http://sgb.gov.br/sace/index_bacias_monitoradas.php?getbacia=btaquari
- http://sace.sgb.gov.br/taquari
- http://rigeo.sgb.gov.br/handle/doc/20099?mode=full



## Aplicações

- https://arkanon.gitlab.io/javascript/enchente/api/street.html



## Maps APIs

### API
- http://developers.google.com/maps/documentation/javascript
- http://developers.google.com/maps/documentation/javascript/streetview
- http://developers.google.com/maps/documentation/javascript/elevation

### Visão Geral
- http://developers.google.com/maps/documentation/javascript/overview
- http://developers.google.com/maps/documentation/streetview/overview
- http://developers.google.com/maps/documentation/elevation/overview

### Exemplo
- http://developers.google.com/maps/documentation/javascript/examples/map-simple
- http://developers.google.com/maps/documentation/javascript/examples/map-coordinates
- http://developers.google.com/maps/documentation/javascript/examples/polygon-simple
- http://developers.google.com/maps/documentation/javascript/examples/streetview-simple
- http://developers.google.com/maps/documentation/javascript/examples/streetview-events
- http://developers.google.com/maps/documentation/javascript/examples/elevation-simple



## Fotos Georeferenciadas

```bash
foto=../fotos/IMG_20240625_151553_045_HDR.jpg
exiftool -s3 -c '%+.6f' -GPSLatitude -GPSLongitude $foto
```



## Referências
- http://usgs.gov/faqs/what-digital-elevation-model-dem



☐
